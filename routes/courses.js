// [Section] Dependencies and Modules
   const exp = require("express");
   const controller = require('../controllers/courses');

// [Section] Routing components
   const route = exp.Router();

// [Section] [POST] Routes
   route.post('/create', (req, res) => {
      let data = req.body;
      controller.createCourse(data).then(outcome => {
      	  res.send(outcome);
      });
   });

// [Section] [GET] Routes
   route.get('/all', (req, res) => {
      controller.getAllCourse().then(outcome => {
        res.send(outcome);
      });
   });

   route.get('/:id', (req, res) => {
      let courseId = req.params.id; 
      controller.getCourse(courseId).then(result => { 
          res.send(result);
      }); 
   }); 

   route.get('/', (req, res) => {
      controller.getAllActiveCourse().then(outcome => {
         res.send(outcome); 
      });
   });

// [Section] [PUT] Routes
  route.put('/:id', (req, res) => {
      let id = req.params.id; 
      let details = req.body;
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;       
      if (cName  !== '' && cDesc !== '' && cCost !== '') {
        controller.updateCourse(id, details).then(outcome => {
            res.send(outcome); 
        });      
      } else {
        res.send('Incorrect Input, Make sure details are complete');
      }
  }); 

  // [Section] Deactivate

  route.put('/:id/archive', (req, res) => {
     let courseId = req.params.id;
     controller.deactivateCourse(courseId).then(resultOfThefunction => {

        res.send(resultOfThefunction);
     });
  });

  //  [Section] Reactivate

  route.put('/:id/reactivate', (req, res) => {
     let courseId = req.params.id;
     controller.reactivateCourse(courseId).then(resultOfThefunction2 => {

        res.send(resultOfThefunction2);
     });
  });



// [Section] [DEL] Routes
  route.delete('/:id', (req, res) => {
     let id = req.params.id; 
     controller.deleteCourse(id).then(outcome => {
        res.send(outcome);
     });
  });



// [Section] Export Route System
   module.exports = route;