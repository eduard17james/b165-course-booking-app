// [Section] Dependencies and Modules
   const Course = require('../models/Course');

// [Section] Functionality [Create]
   module.exports.createCourse = (info) => {
     let cName = info.name;   
     let cDesc = info.description;
     let cCost = info.price;

     let newCourse = new Course({
     	name: cName,
     	description: cDesc,
     	price: cCost
     })

   	 return newCourse.save().then((savedCourse, error) => {
        if (error) {
        	return 'failed to save new Document'
        } else {
        	return savedCourse;
        }
   	 })
   }
// [Section] Functionality [Retrieve]
   module.exports.getAllCourse = () => {
     return Course.find({}).then(outcomeNiFind => {
        return outcomeNiFind;
     });
   };

   module.exports.getCourse = (id) => {
      return Course.findById(id).then(resultOfQuery => {
        return resultOfQuery; 
      });
   };

   module.exports.getAllActiveCourse = () => {
      return Course.find({isActive: true}).then(resultOftheQuery => {
          return resultOftheQuery;
      });
   };

// [Section] Functionality [Update]
   module.exports.updateCourse = (id, details) => {
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;   
      let updatedCourse = {
        name: cName,
        description: cDesc,
        price: cCost
      }
      return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
          if (err) {
            return 'Failed to update Course'; 
          } else {
            return 'Successfully Updated Course'; 
          }
      })
   }

   // [Section] Soft Delete
   
   let updates = {
      isActive: false
   }
   module.exports.deactivateCourse = (id) => {
      
      return Course.findByIdAndUpdate(id, updates).then((archived,error) => {

         if (archived){
            return `The course ${id} has been deactivated`;
         } else {
            return 'Failed to deactivate'
         }
      });
   }


   module.exports.reactivateCourse = (id) => {
      
      let reupdate = {
          isActive: true
      }

      return Course.findByIdAndUpdate(id, reupdate).then((activated,error) => {

         if (activated){
            return `The course ${id} has been reactivated`;
         } else {
            return 'Failed to reactivate'
         }
      });
      
   }



// [Section] Functionality [Delete]
  module.exports.deleteCourse = (id) => {
    return Course.findByIdAndRemove(id).then((removedCourse, err) => {
        if (err) {
          return 'No Course Was Removed'; 
        } else { 
          return 'Course Succesfully Deleted'; 
        };
    });
  };


