// dependencies and modules
  const mongoose = require('mongoose');



// blueprint schema
   const userSchema = new mongoose.Schema({
  	 firstName: {
  	 	type: String,
  	 	required: [true, 'First Name is Required']
  	 }, 
     lastName: {
     	type: String,
     	required: [true, 'Last Name is Required']
     }, 
     email: {
     	type: String,
     	required: [true, 'Email is Required']
     }, 
     password: {
     	type: String,
     	required: [true, 'Password is Required']
     }, 
     mobileNo: {
     	type: String,
     	required: [true, 'Mobile Number is Required']
     }, 
     isAdmin: {
     	type: Boolean,
     	default: false
     },  
     gender: {
     	type: String,
     	required: [true, 'Gender is Required']
     },
     enrollments: [

       {
       	 courseId: {
            type: String,
            required: [true, 'Course ID is Required']
       	 },
       	 enrolledOn: {
            type: Date,
            default: new Date()
       	 },
       	 status: {
            type: String,
            default: 'Enrolled'
       	 }
       }
     ] 
  }) 


// model
   // translate the created schema into mongoose model to be used as interface and perform the CRUD operations.
     // Syntax: model(<COLLECTION/MODEL NAME>,)
   const User = mongoose.model('User', userSchema)
   module.exports = User;